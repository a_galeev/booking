from rest_framework import serializers
from api.models import Table, Booking
from django.db.models import Q


class BookingSerializer(serializers.ModelSerializer):

    def validate(self, attrs):
        if attrs.get('startDate', None) and attrs.get('endDate', None):#по логике требование заполнения полей можно укахать в Meta::extra_kwargs согласно документации; нужно разобраться почему не работает
            if attrs['startDate'] >= attrs['endDate']:
                raise serializers.ValidationError("Дата окончания бронирования должна быть позднее даты её начала")
        else:
            raise serializers.ValidationError("Требуются поля дат")

        if self.instance:
            if attrs.get('seats', None) and attrs['seats'] > self.instance.table.maxSeats:
                raise serializers.ValidationError("Превышено количество доступных мест")

            if self.instance.table.bookings.filter(~Q(id=self.instance.id) & (Q(startDate__range=(attrs['startDate'], attrs['endDate'])) ) | Q(endDate__range=(attrs['startDate'], attrs['endDate']))).exists():
                raise serializers.ValidationError("Данное время для текущего столика уже забронировано")

        return attrs

    def create(self, validated_data):
        av_tables = Table.objects.exclude(Q(maxSeats__lt=validated_data['seats']) | Q(bookings__startDate__range=(validated_data['startDate'], validated_data['endDate'])) | Q(bookings__endDate__range=(validated_data['startDate'], validated_data['endDate'])))
        if not av_tables.exists():
            raise serializers.ValidationError("Абсолютно все столы заняты")

        t_id = av_tables.order_by('maxSeats')[0].id

        return Booking.objects.create(
            seats=validated_data['seats'],
            startDate=validated_data['startDate'],
            endDate=validated_data['endDate'],
            table=Table.objects.get(id=t_id)
            )

    class Meta:
        model = Booking
        fields = ('id', 'seats', 'startDate', 'endDate')
        extra_kwargs = {
            'id': {'read_only': True},
            'seats': {'required': True},
            'startDate': {'required': False},
            'endDate': {'required': False},
        }

class TableSerializer(serializers.ModelSerializer):
    bookings = BookingSerializer(many=True)

    class Meta:
        model = Table
        fields = ('id', 'maxSeats', 'bookings')