from django.urls import path, include
from api.views import *

urlpatterns = [
     path('bookings/', BookingList.as_view(), name="bookings_list"),
     path('bookings/<int:pk>/', BookingDetail.as_view(), name="bookings_detail"),
     path('tables/', TablesListView.as_view(), name="tables_list"), #all tables and bookings list
     path('tables/<int:seats>&<str:startDate>&<str:endDate>/', AvailableTablesList.as_view())   #available tables for current seats and date range
]