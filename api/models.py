from django.db import models
from django.utils.timezone import datetime, now, timedelta

# Create your models here.

class Table(models.Model):
    maxSeats = models.IntegerField(verbose_name='Вместимость стола')

    def __str__(self):
        return "Стол №" + str(self.id)

class Booking(models.Model):
    def default_endDate():
        return datetime.now() + timedelta(hours=2)
#
    seats = models.IntegerField(verbose_name='Число человек', default=1)
    startDate = models.DateTimeField(verbose_name='Время начала', default=now, editable=True, unique=True)
    endDate = models.DateTimeField(verbose_name='Время окончания', default=default_endDate, editable=True, unique=True)
    table = models.ForeignKey(Table, related_name='bookings', on_delete=models.CASCADE)

    def __str__(self):
        return "Бронь №" + str(self.id)