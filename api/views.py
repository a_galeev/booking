from django.shortcuts import render, get_object_or_404
from api.models import Table, Booking
from api.serializers import BookingSerializer, TableSerializer
from rest_framework import generics, viewsets, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.db.models import Q

# Create your views here.

class BookingList(generics.ListCreateAPIView):
    queryset = Booking.objects.all()
    serializer_class = BookingSerializer


class BookingDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Booking.objects.all()
    serializer_class = BookingSerializer
#
# class TableList(generics.ListCreateAPIView):
#     queryset = Table.objects.all()
#     serializer_class = TableSerializer
#
# class TableDetail(generics.RetrieveDestroyAPIView):
#     queryset = Table.objects.all()
#     serializer_class = TableSerializer

class TablesListView(generics.ListCreateAPIView):
    queryset = Table.objects.all()
    serializer_class = TableSerializer

class AvailableTablesList(generics.ListAPIView):
    serializer_class = TableSerializer

    def get_queryset(self):
        seats = self.kwargs['seats']
        startDate = self.kwargs['startDate']
        endDate = self.kwargs['endDate']

        if startDate >= endDate:
            raise generics.ValidationError("Дата окончания бронирования должна быть позднее даты её начала")

        return Table.objects.exclude(Q(maxSeats__lt=seats) | Q(bookings__startDate__range=(startDate, endDate)) | Q(bookings__endDate__range=(startDate, endDate))).order_by('maxSeats')












